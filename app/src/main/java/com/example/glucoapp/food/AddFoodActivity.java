package com.example.glucoapp.food;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.glucoapp.DTO.FoodDTO;
import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.MenuActivity;
import com.example.glucoapp.R;
import com.example.glucoapp.support.DatePickerSupClass;
import com.example.glucoapp.support.PostObjectSup;
import com.example.glucoapp.support.TimePickerSupClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class AddFoodActivity extends AppCompatActivity {

    private UserDTO user;

    EditText nameField;
    EditText quantityField;
    EditText timeField;
    EditText dateField;
    TextView quantityTextView;
    RadioButton foodRB;
    RadioButton drinkRB;
    RadioGroup typeRadioGroup;
    String type;
    String unitOfQuantity;

    Calendar myCalendar = Calendar.getInstance();
    String typeInPolish;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_food_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setup();

    }


    public void onClickAddFood(View view) throws InterruptedException {

        if (!nameField.getText().toString().isEmpty() && !quantityField.getText().toString().isEmpty()) {

            FoodDTO f = foodBuilder();
            PostObjectSup pos = new PostObjectSup(f);
            int response = pos.postObject();
            if (response == 1) {
                onAddSuccess();
            } else if (response == 2) {
                Toast.makeText(this, typeInPolish+" został już wcześniej zapisany",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Wszystkie pola muszą być wypełnione",
                    Toast.LENGTH_SHORT).show();
        }

    }


    public void onClickBack(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        onClickBack(findViewById(R.id.backBtn));
    }

    public FoodDTO foodBuilder() {
        LocalDate date = LocalDate.parse(dateField.getText().toString());
        LocalTime time = LocalTime.parse(timeField.getText().toString());
        LocalDateTime dateTime = LocalDateTime.of(date, time);

        FoodDTO foodDTO = new FoodDTO(user, type, nameField.getText().toString(), Integer.parseInt(quantityField.getText().toString())
                , unitOfQuantity, dateTime);


        return foodDTO;
    }

    public void onAddSuccess() {
            Toast.makeText(this, typeInPolish+" został zapisany pomyślnie",
                    Toast.LENGTH_SHORT).show();

    }

    public void setup() {

        nameField = findViewById(R.id.nameField);
        quantityField = findViewById(R.id.quantityField);
        timeField = findViewById(R.id.timeField);
        dateField = findViewById(R.id.dateField);
        user = (UserDTO) getIntent().getSerializableExtra("user");
        drinkRB = findViewById(R.id.drinkRB);
        foodRB = findViewById(R.id.foodRB);
        quantityTextView = findViewById(R.id.quantityTextView);
        typeRadioGroup = findViewById(R.id.typeRadioGroup);


        quantityField.setHint("objętość [ml]");
        quantityTextView.setText("Objętość [ml]");
        type = "drink";
        unitOfQuantity = "ml";

        if(type.equals("drink")){
            typeInPolish="Napój";
        }else {
            typeInPolish="Posiłek";
        }

        LocalDate dateNow = LocalDate.now();
        LocalTime timeNow = LocalTime.now();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        timeField.setText(timeNow.format(dtf));
        dateField.setText(dateNow.toString());

        DatePickerSupClass dp = new DatePickerSupClass(myCalendar, dateField, this);
        dp.dataPicker();
        TimePickerSupClass tp = new TimePickerSupClass(myCalendar, timeField, this);
        tp.timePicker();

        typeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.drinkRB) {
                    quantityField.setHint("objętość [ml]");
                    quantityTextView.setText("Objętość [ml]");
                    type = "drink";
                    unitOfQuantity = "ml";
                    typeInPolish="Napój";
                } else {
                    quantityField.setHint("masa [g]");
                    quantityTextView.setText("Masa [g]");
                    type = "food";
                    typeInPolish="Posiłek";
                    unitOfQuantity = "g";
                }
            }
        });


    }
}
