package com.example.glucoapp.food;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.glucoapp.DTO.FoodDTO;
import com.example.glucoapp.DTO.FoodDTO;
import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.MenuActivity;
import com.example.glucoapp.R;
import com.example.glucoapp.support.DatePickerSupClass;
import com.example.glucoapp.support.FoodListAdapter;
import com.example.glucoapp.support.GetObjectsListSup;
import com.example.glucoapp.support.GlucoseListAdapter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

public class FoodHistoryActivity extends AppCompatActivity {

    EditText dateField;
    ListView foodListView;
    Calendar myCalendar=Calendar.getInstance();
    private UserDTO user=new UserDTO();
    List<FoodDTO> foodDTOS;
    TextView noDataText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_history_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        user = (UserDTO) getIntent().getSerializableExtra("user");
        try {
            setup();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onClickBack(View view) {
        Intent intent =new Intent(this, MenuActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        onClickBack(findViewById(R.id.backBtn));
    }

    public void setup() throws InterruptedException {
        dateField=findViewById(R.id.dateField);
        foodListView=findViewById(R.id.foodList);
        noDataText=findViewById(R.id.noDataText);
        LocalDate dateNow=LocalDate.now();
        dateField.setText(dateNow.toString());

        DatePickerSupClass dp=new DatePickerSupClass(myCalendar,dateField,this);
        dp.dataPicker();

        foodDTOS=getFoodList();
        historyShow();

        dateField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                historyShow();
            }
        });

    }

    public List<FoodDTO> getFoodList() throws InterruptedException {
        GetObjectsListSup getObjectsListSup=new GetObjectsListSup(user);
        FoodDTO[] fArray=getObjectsListSup.downloadFoods();
        List<FoodDTO> fList=new ArrayList<>();

        if(fArray!=null) {
            fList.addAll(Arrays.asList(fArray));

            return fList;
        }
        return null;
    }

    public void historyShow(){
        ArrayList<FoodDTO> dayFoodList=new ArrayList<>();
        LocalDate typedDate=LocalDate.parse(dateField.getText().toString());
        if(foodDTOS!=null) {
            for (FoodDTO f : foodDTOS) {
                LocalDate fDate = LocalDate.of(f.getDateTime().getYear()
                        , f.getDateTime().getMonthValue()
                        , f.getDateTime().getDayOfMonth());
                if (fDate.equals(typedDate)) {
                    dayFoodList.add(f);
                }
            }
        }



        if(dayFoodList.size()==0){
            noDataText.setVisibility(View.VISIBLE);
            foodListView.setVisibility(View.INVISIBLE);
        }else {
            noDataText.setVisibility(View.INVISIBLE);
            foodListView.setVisibility(View.VISIBLE);
            Comparator<FoodDTO> comparator=new Comparator<FoodDTO>() {
                @Override
                public int compare(FoodDTO o1, FoodDTO o2) {
                    return o1.getDateTime().compareTo(o2.getDateTime());
                }
            };
            dayFoodList.sort(comparator);
            FoodListAdapter adapter = new FoodListAdapter(getApplicationContext(), dayFoodList);
            foodListView.setAdapter(adapter);
        }
    }
}
