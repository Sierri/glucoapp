package com.example.glucoapp.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

public class FoodDTO {

    private UserDTO userDTO;
    private String Type;
    private String name;
    private int quantity;
    private String unitOfQuntity;

    @JsonProperty("dateTime")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateTime;

    public FoodDTO() {
    }

    public FoodDTO(UserDTO userDTO, String type, String name, int quantity, String unitOfQuntity, LocalDateTime dateTime) {
        this.userDTO = userDTO;
        Type = type;
        this.name = name;
        this.quantity = quantity;
        this.unitOfQuntity = unitOfQuntity;
        this.dateTime = dateTime;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnitOfQuntity() {
        return unitOfQuntity;
    }

    public void setUnitOfQuntity(String unitOfQuntity) {
        this.unitOfQuntity = unitOfQuntity;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "FoodDTO{" +
                "userDTO=" + userDTO +
                ", Type='" + Type + '\'' +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", unitOfQuntity='" + unitOfQuntity + '\'' +
                ", dateTime=" + dateTime +
                '}';
    }
}
