package com.example.glucoapp.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;


import java.time.LocalDateTime;

@JsonIgnoreProperties
public class GlucoseMeasureDTO {

    @JsonProperty("userDTO")
    private UserDTO userDTO;
    @JsonProperty("measure")
    private Double measure;

    @JsonProperty("measureDateTime")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime measureDateTime;

    public GlucoseMeasureDTO() {
    }

    public GlucoseMeasureDTO(UserDTO userDTO, Double measure, LocalDateTime measureDateTime) {
        this.userDTO = userDTO;
        this.measure = measure;
        this.measureDateTime = measureDateTime;
    }

    @Override
    public String toString() {
        return "GlucoseMeasureDTO{" +
                "userDTO=" + userDTO +
                ", measure=" + measure +
                ", measureDateTime=" + measureDateTime +
                '}';
    }


    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public Double getMeasure() {
        return measure;
    }

    public void setMeasure(Double measure) {
        this.measure = measure;
    }

    public LocalDateTime getMeasureDateTime() {
        return measureDateTime;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setMeasureDateTime(LocalDateTime measureDateTime) {
        this.measureDateTime = measureDateTime;
    }


}
