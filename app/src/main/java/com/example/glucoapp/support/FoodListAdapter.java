package com.example.glucoapp.support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.glucoapp.DTO.FoodDTO;
import com.example.glucoapp.R;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class FoodListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<FoodDTO> arrayList;
    private TextView nameTextView, timeTextView, typeTextView, quantityTextView;

    public FoodListAdapter(Context context, ArrayList<FoodDTO> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.single_item_list_layout2, parent, false);
        timeTextView = convertView.findViewById(R.id.timeTextView);
        nameTextView = convertView.findViewById(R.id.nameTextView);
        quantityTextView = convertView.findViewById(R.id.quantityTextView);
        typeTextView = convertView.findViewById(R.id.typeTextView);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time = LocalTime.of(arrayList.get(position).getDateTime().getHour(), arrayList.get(position).getDateTime().getMinute());
        timeTextView.setText(time.format(dtf));
        nameTextView.setText(arrayList.get(position).getName());
        String type = arrayList.get(position).getType();
        if (type.equals("drink")) {
            typeTextView.setText(R.string.drink);
            quantityTextView.setText("(" + arrayList.get(position).getQuantity() + " ml)");
        } else {
            typeTextView.setText(R.string.food);
            quantityTextView.setText("(" + arrayList.get(position).getQuantity() + " g)");
        }


        return convertView;
    }
}
