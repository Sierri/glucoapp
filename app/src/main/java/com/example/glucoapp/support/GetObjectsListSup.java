package com.example.glucoapp.support;

import com.example.glucoapp.DTO.FoodDTO;
import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.DTO.UserDTO;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class GetObjectsListSup {

    private UserDTO userDTO;
    private String url;

    public GetObjectsListSup(UserDTO userDTO) {
        this.userDTO = userDTO;

    }

    private GlucoseMeasureDTO[] gList;
    public GlucoseMeasureDTO[] downloadMeasures() throws InterruptedException {
        url = "http://10.0.2.2:8080/api/glucose/all?username=" + userDTO.getUsername() + "&password=" + userDTO.getPassword();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    gList = restTemplate.getForObject(url, GlucoseMeasureDTO[].class);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        thread.join();
        return gList;
    }

    private FoodDTO[] foodDTOS;

    public FoodDTO[] downloadFoods() throws InterruptedException {
        url = "http://10.0.2.2:8080/api/food/all?username=" + userDTO.getUsername() + "&password=" + userDTO.getPassword();


        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    //Your code goes here


                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    foodDTOS = restTemplate.getForObject(url, FoodDTO[].class);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        thread.join();

        return foodDTOS;
    }


}
