package com.example.glucoapp.support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.R;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class GlucoseListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<GlucoseMeasureDTO> arrayList;
    private TextView timeTextView, valueTextView;

    public GlucoseListAdapter(Context context, ArrayList<GlucoseMeasureDTO> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MeasureInterpreter measureInterpreter = new MeasureInterpreter(arrayList.get(position).getMeasure());
        int status = measureInterpreter.measureStatus();
        convertView = LayoutInflater.from(context).inflate(R.layout.single_item_list_layout, parent, false);

        timeTextView = convertView.findViewById(R.id.timeTextView);
        valueTextView = convertView.findViewById(R.id.valueTextView);

        if (status == 0) {
            valueTextView.setBackground(context.getDrawable(R.drawable.blue_text_theme));
        } else if (status == 1) {
            valueTextView.setBackground(context.getDrawable(R.drawable.green_text_theme2));
        } else if (status == 2) {
            valueTextView.setBackground(context.getDrawable(R.drawable.orange_text_theme));
        } else {
            valueTextView.setBackground(context.getDrawable(R.drawable.red_text_theme));
        }


        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime selectedTime = LocalTime.of(arrayList.get(position).getMeasureDateTime().getHour(), arrayList.get(position).getMeasureDateTime().getMinute());
        timeTextView.setText(selectedTime.format(dtf));

        valueTextView.setText(arrayList.get(position).getMeasure().toString() + " [mg/dl]");
        return convertView;
    }
}



