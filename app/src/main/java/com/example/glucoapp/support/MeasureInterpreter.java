package com.example.glucoapp.support;

import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.R;

public class MeasureInterpreter {
    private double value;

    public MeasureInterpreter(double value) {
        this.value = value;
    }

    public int measureStatus() {
        int status;

        if (value < 70) {
            status = 0;
        } else if (value >= 70 && value < 100) {
            status = 1;
        } else if (value >= 100 && value < 126) {

            status = 2;
        } else {

            status = 3;
        }

        return status;
    }
}
