package com.example.glucoapp.support;

import android.util.Log;

import com.example.glucoapp.DTO.FoodDTO;
import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.DTO.UserDTO;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.json.JSONException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class PostObjectSup {

    private Object objectDTO;
    private String url;


    public PostObjectSup(UserDTO userDTO) {
        this.url = "http://10.0.2.2:8080/api/user";
        this.objectDTO = userDTO;
    }

    public PostObjectSup(GlucoseMeasureDTO glucoseMeasureDTO) {
        this.url = "http://10.0.2.2:8080/api/glucose";
        this.objectDTO = glucoseMeasureDTO;
    }

    public PostObjectSup(FoodDTO foodDTO) {
        this.url = "http://10.0.2.2:8080/api/food";
        this.objectDTO = foodDTO;
    }

    int response = 0;

    public int postObject() throws InterruptedException {


        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    response = save();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        thread.join();

        Log.d("---------RESPONSE--------", String.valueOf(response));
        return response;

    }

    public int save() throws JSONException {


        RestTemplate restTemplate = new RestTemplate();

        MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jackson2HttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        restTemplate.getMessageConverters().add(jackson2HttpMessageConverter);

        int response = restTemplate.postForObject(url, objectDTO, Integer.class);
        return response;

    }

}
