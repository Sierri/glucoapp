package com.example.glucoapp.support;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Locale;

public class TimePickerSupClass {
    private Calendar myCalendar;
    private EditText timeField;
    private Context context;


    public TimePickerSupClass(Calendar myCalendar, EditText timeField, Context context) {
        this.myCalendar = myCalendar;
        this.timeField = timeField;
        this.context = context;
    }

    public void timePicker() {


        timeField.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
                        LocalTime selectedTime = LocalTime.of(selectedHour, selectedMinute);
                        timeField.setText(selectedTime.format(dtf));
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


    }


}
