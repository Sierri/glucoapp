package com.example.glucoapp.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.MenuActivity;
import com.example.glucoapp.R;

import org.springframework.http.client.CommonsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class LoginActivity extends AppCompatActivity {

    private UserDTO userDTOTyped = new UserDTO();
    private UserDTO userDTOResponse = new UserDTO();

    EditText usernameField;
    EditText passwordField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        usernameField = findViewById(R.id.userField);
        passwordField = findViewById(R.id.passwordField);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void onClickLogIn(View view) throws InterruptedException {


        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();

        loginAttempt(username, password);


    }

    public void onClickRegister(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        this.startActivity(intent);
    }

    private void loginAttempt(String username, String password) throws InterruptedException {

        if (username.length() == 0 || password.length() == 0) {
            Toast.makeText(LoginActivity.this.getApplicationContext(), "Wszystkie pola muszą zostać wypełnione",
                    Toast.LENGTH_SHORT).show();
        } else {
            userDTOTyped = new UserDTO(username, password);
            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        //Your code goes here
                        userDTOResponse = FindUser();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            thread.join();


            if (userDTOResponse != null) {
                Intent intent = new Intent(this, MenuActivity.class);
                intent.putExtra("user", userDTOResponse);
                this.startActivity(intent);
            } else {
                Toast.makeText(LoginActivity.this.getApplicationContext(), "Zła nazwa użytkownika lub hasło.\nSpróbuj ponownie",
                        Toast.LENGTH_SHORT).show();

            }

        }

    }


    private UserDTO FindUser() {
        String username = userDTOTyped.getUsername();
        String password = userDTOTyped.getPassword();

        //10.0.2.2 - most pomiędzy emulatorem a serwerem lokalnym na komputerze
        String url = "http://10.0.2.2:8080/api/user?username={username}&password={password}";
        RestTemplate restTemplate = new RestTemplate();

        // Add the String message converter
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        userDTOResponse = restTemplate.getForObject(url, UserDTO.class, username, password);

        return userDTOResponse;

    }


}
