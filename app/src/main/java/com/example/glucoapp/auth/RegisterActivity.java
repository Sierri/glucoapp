package com.example.glucoapp.auth;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.R;
import com.example.glucoapp.support.PostObjectSup;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class RegisterActivity extends AppCompatActivity {

    private int response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void onClickRegister1(View view) throws InterruptedException {
        EditText usernameEdt = findViewById(R.id.usernameField);
        EditText passwordEdt = findViewById(R.id.passwordField);
        EditText confirmPassEdt = findViewById(R.id.confirmPasswordField);

        String username = usernameEdt.getText().toString();
        String password = passwordEdt.getText().toString();
        String confPass = confirmPassEdt.getText().toString();

        savingAttempt(username, password, confPass);
    }


    public void onClickBack(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        this.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        onClickBack(findViewById(R.id.backBtn));
    }

    public void savingAttempt(String username, String password, String confPass) throws InterruptedException {

        if (!(username.isEmpty() || password.isEmpty() || confPass.isEmpty())) {
            if (!(username.length() < 6 || username.length() > 12)) {
                if (!(password.length() < 6)) {
                    if (password.equals(confPass)) {

                        UserDTO userTyped = new UserDTO(username, password);
                        response = 0;
                        PostObjectSup pos = new PostObjectSup(userTyped);
                        response = pos.postObject();
//
                        if (response == 1) {
                            Toast.makeText(RegisterActivity.this.getApplicationContext(), "Poprawnie dodano użytkownika",
                                    Toast.LENGTH_SHORT).show();
                            registerSuccess();

                        } else if (response == 2) {
                            Toast.makeText(RegisterActivity.this.getApplicationContext(), "Użytkownik już istnieje",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RegisterActivity.this.getApplicationContext(), "Coś poszło nie tak.",
                                    Toast.LENGTH_SHORT).show();
                        }


                    } else
                        Toast.makeText(RegisterActivity.this.getApplicationContext(), "Hasło niepoprawnie powtórzone.",
                                Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(RegisterActivity.this.getApplicationContext(), "Hasło musi sie składać z przynajmniej 6 znaków.",
                            Toast.LENGTH_SHORT).show();

            } else
                Toast.makeText(RegisterActivity.this.getApplicationContext(), "Nazwa użytkownika musi się składać z od 6 do 12 znaków.",
                        Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(RegisterActivity.this.getApplicationContext(), "Wszystkie pola muszą zostać wypełnione.",
                    Toast.LENGTH_SHORT).show();


    }


    public void registerSuccess() {
        LinearLayout registryLayout = findViewById(R.id.registryLinearLayout);
        registryLayout.setVisibility(View.INVISIBLE);

    }

}
