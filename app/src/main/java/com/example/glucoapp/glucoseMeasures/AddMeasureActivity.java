package com.example.glucoapp.glucoseMeasures;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.MenuActivity;
import com.example.glucoapp.R;
import com.example.glucoapp.support.DatePickerSupClass;
import com.example.glucoapp.support.MeasureInterpreter;
import com.example.glucoapp.support.PostObjectSup;
import com.example.glucoapp.support.TimePickerSupClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class AddMeasureActivity extends AppCompatActivity {


    private UserDTO user = new UserDTO();

    EditText valueField;
    EditText dateField;
    EditText timeField;
    RadioButton mgRBt;
    RadioButton mmolRBt;
    TextView checkText;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_measure_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        user = (UserDTO) getIntent().getSerializableExtra("user");
        setup();
    }

    public void onClickCheckMeasure(View view) {
        String value = valueField.getText().toString();
        if (!value.isEmpty()) {
            checkValue(value);
        } else {
            Toast.makeText(this, "Podaj wartość pomiaru",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickSaveMeasure(View view) throws InterruptedException {
        String value = valueField.getText().toString();
        if (!value.isEmpty()) {
            double mgValue = checkValue(value);
            GlucoseMeasureDTO g = measureBuilder(mgValue);
            PostObjectSup pos = new PostObjectSup(g);
            int response = pos.postObject();
            if (response == 1) {
                onAddSuccess();
            } else if (response == 2) {
                Toast.makeText(this, "Podany pomiar był już zapisywany",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Podaj wartość pomiaru",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void onClickBack(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        onClickBack(findViewById(R.id.backBtn));
    }


    public void setup() {
        valueField = findViewById(R.id.valueField);
        dateField = findViewById(R.id.dateField);
        timeField = findViewById(R.id.timeField);
        mgRBt = findViewById(R.id.unitRBt1);
        mmolRBt = findViewById(R.id.unitRBt2);
        checkText = findViewById(R.id.checkText);

        checkText.setVisibility(View.INVISIBLE);
        String dateNow = LocalDate.now().toString();
        LocalTime timeNow = LocalTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        timeField.setText(timeNow.format(dtf));
        dateField.setText(dateNow);


        DatePickerSupClass dp = new DatePickerSupClass(myCalendar, dateField, this);
        dp.dataPicker();
        TimePickerSupClass tp = new TimePickerSupClass(myCalendar, timeField, this);
        tp.timePicker();


    }

    public double checkValue(String valueString) {
        int value = Integer.parseInt(valueString);
        double valueInMgOnDl;
        if (mgRBt.isChecked()) {
            valueInMgOnDl = value;
        } else {
            valueInMgOnDl = value * 18;             //1 [mmol/l] = 18 [mg/dl]
        }


        MeasureInterpreter measureInterpreter = new MeasureInterpreter(valueInMgOnDl);
        int statusInt = measureInterpreter.measureStatus();
        String status;

        if (statusInt == 0) {
            status = "Zbyt niski poziom cukru";
            checkText.setBackground(getDrawable(R.drawable.blue_text_theme));
        } else if (statusInt == 1) {
            checkText.setBackground(getDrawable(R.drawable.green_text_theme2));
            status = "Prawidłowy poziom cukru";
        } else if (statusInt == 2) {
            checkText.setBackground(getDrawable(R.drawable.orange_text_theme));
            status = "Podwyższony poziom cukru";
        } else {
            checkText.setBackground(getDrawable(R.drawable.red_text_theme));
            status = "Zbyt wysoki poziom cukru";
        }

        checkText.setVisibility(View.VISIBLE);
        checkText.setText(status);
        return valueInMgOnDl;

    }

    public GlucoseMeasureDTO measureBuilder(Double value) {
        String dateString = dateField.getText().toString();
        LocalDate date = LocalDate.parse(dateString);
        String timeString = timeField.getText().toString();
        LocalTime time = LocalTime.parse(timeString);
        LocalDateTime dateTime = LocalDateTime.of(date, time);

        GlucoseMeasureDTO glucoseMeasureDTO = new GlucoseMeasureDTO(user, value, dateTime);

        return glucoseMeasureDTO;
    }


    public void onAddSuccess() {
        Toast.makeText(this, "Pomiar zapisany pomyślnie",
                Toast.LENGTH_SHORT).show();

    }


}