package com.example.glucoapp.glucoseMeasures;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.MenuActivity;
import com.example.glucoapp.R;
import com.example.glucoapp.support.DatePickerSupClass;
import com.example.glucoapp.support.GetObjectsListSup;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MeasureChartActivity extends AppCompatActivity {

    Calendar myCalendar = Calendar.getInstance();
    UserDTO user = new UserDTO();
    List<GlucoseMeasureDTO> glucoseMeasureDTOS;
    Spinner weekSpinner;
    EditText dateField;
    LineChart lineChart;
    RadioGroup timeRadioGroup;
    RadioButton dayRBt;
    RadioButton weekRBt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.measure_chart_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        user = (UserDTO) getIntent().getSerializableExtra("user");

        try {
            Setup();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onClickBack(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        onClickBack(findViewById(R.id.backBtn));
    }


    public void Setup() throws InterruptedException {
        weekSpinner = findViewById(R.id.dateSpinner);
        dateField = findViewById(R.id.dateField);
        lineChart = findViewById(R.id.glucoseChart);

        timeRadioGroup = findViewById(R.id.timeRadioGroup);
        dayRBt = findViewById(R.id.dayRBt);
        weekRBt = findViewById(R.id.weekRBt);

        LocalDate dateNow = LocalDate.now();
        dateField.setText(dateNow.toString());

        DatePickerSupClass dp = new DatePickerSupClass(myCalendar, dateField, this);
        dp.dataPicker();

        glucoseMeasureDTOS = getMeasuresList();
        dayChartCreator();
        listenersSetup();


    }

    public List<GlucoseMeasureDTO> getMeasuresList() throws InterruptedException {
        GetObjectsListSup getObjectsListSup = new GetObjectsListSup(user);
        GlucoseMeasureDTO[] gArray = getObjectsListSup.downloadMeasures();
        List<GlucoseMeasureDTO> gList = new ArrayList<>();

        if (gArray != null) {
            gList.addAll(Arrays.asList(gArray));

            return gList;
        }
        return null;
    }

    public void dayChartCreator() {

        double maxValue = 0;

        ArrayList<Entry> dataToChart = new ArrayList<>();
        List<GlucoseMeasureDTO> dayMeasuresList = new ArrayList<>();
        LocalDate typedDate = LocalDate.parse(dateField.getText().toString());
        for (GlucoseMeasureDTO g : glucoseMeasureDTOS) {
            LocalDate gDate = LocalDate.of(g.getMeasureDateTime().getYear()
                    , g.getMeasureDateTime().getMonthValue()
                    , g.getMeasureDateTime().getDayOfMonth());
            if (gDate.equals(typedDate)) {
                dayMeasuresList.add(g);
                LocalTime gTime = LocalTime.of(g.getMeasureDateTime().getHour(), g.getMeasureDateTime().getMinute());
                dataToChart.add(new Entry(gTime.toSecondOfDay(), g.getMeasure().floatValue()));

                if (maxValue < g.getMeasure()) {
                    maxValue = g.getMeasure();
                }

            }
        }

        Comparator<Entry> comparator = new Comparator<Entry>() {
            @Override
            public int compare(Entry o1, Entry o2) {
                return Float.compare(o1.getX(), o2.getX());
            }
        };

        dataToChart.sort(comparator);

        lineChart.getAxisLeft().setAxisMaximum((float) (maxValue + 20));

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        LineDataSet lineDataSet = new LineDataSet(dataToChart, "Measures");
        lineDataSet = dataSetAppearance(lineDataSet);

        dataSets.add(lineDataSet);
        LineData lineData = new LineData(dataSets);
        lineChart.setData(lineData);
        lineChart.invalidate();
        lineChart.getDescription().setEnabled(false);
        configureLineChart(1);


    }

    public void weekChartCreator(int selectedWeek) {

        double maxValue = 0;
        LocalDate endDate;
        LocalDate startDate;


        List<GlucoseMeasureDTO> weekMeasuresList = new ArrayList<>();
        LocalDate now = LocalDate.now();
        if (selectedWeek == 1) {
            endDate = now;
            startDate = now.minusDays(6);
        } else if (selectedWeek == 2) {
            endDate = now.minusDays(7);
            startDate = now.minusDays(13);
        } else if (selectedWeek == 3) {
            endDate = now.minusDays(14);
            startDate = now.minusDays(20);
        } else if (selectedWeek == 4) {
            endDate = now.minusDays(21);
            startDate = now.minusDays(27);
        } else {
            endDate = now.minusDays(28);
            startDate = now.minusDays(34);
        }


        for (GlucoseMeasureDTO g : glucoseMeasureDTOS) {
            LocalDate gDate = LocalDate.of(g.getMeasureDateTime().getYear()
                    , g.getMeasureDateTime().getMonthValue(), g.getMeasureDateTime().getDayOfMonth());
            if ((gDate.isEqual(startDate) || gDate.isAfter(startDate)) && (gDate.isEqual(endDate) || (gDate.isBefore(endDate)))) {
                weekMeasuresList.add(g);
            }
        }

        ArrayList<Entry> dataToChart = new ArrayList<>();
        LocalDate date = startDate;

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setAxisMinimum(startDate.toEpochDay());
        xAxis.setAxisMaximum(endDate.toEpochDay());
        while (date.isBefore(endDate) || date.isEqual(endDate)) {
            List<Double> dayMeasures = new ArrayList<>();

            for (GlucoseMeasureDTO g : weekMeasuresList) {
                if (g.getMeasureDateTime().getDayOfYear() == date.getDayOfYear()) {
                    dayMeasures.add(g.getMeasure());
                }
            }

            float avgOfDay = 0;
            if (dayMeasures.size() > 0) {
                double sumValue = 0;
                for (double value : dayMeasures) {
                    sumValue += value;
                }
                avgOfDay = (float) (sumValue / dayMeasures.size());
            }


            float x = date.toEpochDay();
            float y = avgOfDay;
            if (y != 0) dataToChart.add(new Entry(x, y));

            if (avgOfDay > maxValue) {
                maxValue = avgOfDay;
            }
            date = date.plusDays(1L);
        }


        Comparator<Entry> eComparator = new Comparator<Entry>() {
            @Override
            public int compare(Entry o1, Entry o2) {
                return Float.compare(o1.getX(), o2.getX());
            }
        };

        dataToChart.sort(eComparator);

        lineChart.getAxisLeft().setAxisMaximum((float) (maxValue + 20));


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        LineDataSet lineDataSet = new LineDataSet(dataToChart, "Measures");
        lineDataSet = dataSetAppearance(lineDataSet);

        dataSets.add(lineDataSet);
        LineData lineData = new LineData(dataSets);
        lineChart.setData(lineData);
        lineChart.getDescription().setEnabled(false);
        configureLineChart(2);
        lineChart.invalidate();


    }

    public void configureLineChart(int chartType) {
        //wygląd lewej osi y
        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setTextSize(12);
        leftAxis.setEnabled(true);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setGridColor(Color.BLACK);

        YAxis yAxisRight = lineChart.getAxisRight();
        yAxisRight.setEnabled(false);
        //wygląd osi x
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setTextSize(12);
        xAxis.setGridColor(Color.BLACK);

        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDrawGridBackground(false);
        lineChart.setHighlightPerDragEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.TOP);


        if (chartType == 1) {                                           //formatowanie dla wykresu dniowego
            xAxis.setAxisMinimum(0f);
            xAxis.setLabelCount(5);

            xAxis.setAxisMaximum(TimeUnit.HOURS.toSeconds(24));

            xAxis.setValueFormatter(new ValueFormatter() {
                private final SimpleDateFormat mFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

                @Override
                public String getFormattedValue(float value) {

                    long millis = (long) value * 1000 - 60 * 60 * 1000;
                    return mFormat.format(new Date(millis));
                }
            });


        } else {                                                     // formatowanie dla wykresu tygodniowego
            xAxis.setLabelCount(5);
            xAxis.setValueFormatter(new ValueFormatter() {
                private final SimpleDateFormat mFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());

                @Override
                public String getFormattedValue(float value) {
                    long millis = TimeUnit.DAYS.toMillis((long) value);
                    return mFormat.format(new Date(millis));
                }
            });
        }
    }

    public void listenersSetup() {

        dateField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                dayChartCreator();
            }

        });

        weekSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int selectedWeek = 1;

                switch (position) {
                    case 0:
                        selectedWeek = 1;
                        break;
                    case 1:
                        selectedWeek = 2;
                        break;
                    case 2:
                        selectedWeek = 3;
                        break;
                    case 3:
                        selectedWeek = 4;
                        break;
                    case 4:
                        selectedWeek = 5;
                        break;

                }

                if (weekRBt.isChecked()) {
                    weekChartCreator(selectedWeek);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        timeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.dayRBt) {
                    weekSpinner.setVisibility(View.INVISIBLE);
                    dateField.setVisibility(View.VISIBLE);
                    weekSpinner.setClickable(false);
                    dateField.setClickable(true);
                    dayChartCreator();
                } else {
                    weekSpinner.setVisibility(View.VISIBLE);
                    dateField.setVisibility(View.INVISIBLE);
                    dateField.setClickable(false);
                    weekSpinner.setClickable(true);
                    weekChartCreator(weekSpinner.getSelectedItemPosition() + 1);
                }
            }
        });

    }

    public LineDataSet dataSetAppearance(LineDataSet lineDataSet) {

        lineDataSet.setDrawCircles(true);
        lineDataSet.setCircleRadius(8);
        lineDataSet.setValueTextSize(16);
        lineDataSet.setValueTextColor(Color.WHITE);
        lineDataSet.setLineWidth(3);
        lineDataSet.setColor(Color.BLACK);
        lineDataSet.setCircleColor(Color.BLACK);

        return lineDataSet;
    }


}


