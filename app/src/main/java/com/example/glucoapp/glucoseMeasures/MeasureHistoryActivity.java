package com.example.glucoapp.glucoseMeasures;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.glucoapp.DTO.GlucoseMeasureDTO;
import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.MenuActivity;
import com.example.glucoapp.R;
import com.example.glucoapp.support.DatePickerSupClass;
import com.example.glucoapp.support.GetObjectsListSup;
import com.example.glucoapp.support.GlucoseListAdapter;
import com.github.mikephil.charting.data.Entry;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;


public class MeasureHistoryActivity extends AppCompatActivity {

    EditText dateField;
    ListView glucoseListView;
    Calendar myCalendar = Calendar.getInstance();
    UserDTO user = new UserDTO();
    List<GlucoseMeasureDTO> glucoseMeasureDTOS;
    TextView noDataTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.measure_history_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        user = (UserDTO) getIntent().getSerializableExtra("user");
        try {
            setup();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void onClickBack(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        onClickBack(findViewById(R.id.backBtn));
    }

    public void setup() throws InterruptedException {
        dateField = findViewById(R.id.dateField);
        glucoseListView = findViewById(R.id.glucoseList);
        noDataTextView = findViewById(R.id.noDataText);
        LocalDate dateNow = LocalDate.now();
        dateField.setText(dateNow.toString());


        DatePickerSupClass dp = new DatePickerSupClass(myCalendar, dateField, this);
        dp.dataPicker();

        glucoseMeasureDTOS = getMeasuresList();
        historyShow();

        dateField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                historyShow();
            }
        });

    }

    public List<GlucoseMeasureDTO> getMeasuresList() throws InterruptedException {
        GetObjectsListSup getObjectsListSup = new GetObjectsListSup(user);
        GlucoseMeasureDTO[] gArray = getObjectsListSup.downloadMeasures();
        List<GlucoseMeasureDTO> gList = new ArrayList<>();

        if (gArray != null) {
            gList.addAll(Arrays.asList(gArray));

            return gList;
        }
        return null;
    }

    public void historyShow() {
        ArrayList<GlucoseMeasureDTO> dayMeasuresList = new ArrayList<>();
        LocalDate typedDate = LocalDate.parse(dateField.getText().toString());
        for (GlucoseMeasureDTO g : glucoseMeasureDTOS) {
            LocalDate gDate = LocalDate.of(g.getMeasureDateTime().getYear()
                    , g.getMeasureDateTime().getMonthValue()
                    , g.getMeasureDateTime().getDayOfMonth());
            if (gDate.equals(typedDate)) {
                dayMeasuresList.add(g);
            }
        }

        if (dayMeasuresList.size() == 0) {
            noDataTextView.setVisibility(View.VISIBLE);
            glucoseListView.setVisibility(View.INVISIBLE);
        } else {
            noDataTextView.setVisibility(View.INVISIBLE);
            glucoseListView.setVisibility(View.VISIBLE);

            Comparator<GlucoseMeasureDTO> comparator = new Comparator<GlucoseMeasureDTO>() {
                @Override
                public int compare(GlucoseMeasureDTO o1, GlucoseMeasureDTO o2) {
                    return o1.getMeasureDateTime().compareTo(o2.getMeasureDateTime());
                }
            };

            dayMeasuresList.sort(comparator);

            GlucoseListAdapter adapter = new GlucoseListAdapter(getApplicationContext(), dayMeasuresList);
            glucoseListView.setAdapter(adapter);
        }
    }


}
