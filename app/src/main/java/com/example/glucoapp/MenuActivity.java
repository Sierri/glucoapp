package com.example.glucoapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.glucoapp.DTO.UserDTO;
import com.example.glucoapp.glucoseMeasures.AddMeasureActivity;
import com.example.glucoapp.glucoseMeasures.MeasureChartActivity;
import com.example.glucoapp.glucoseMeasures.MeasureHistoryActivity;
import com.example.glucoapp.auth.LoginActivity;
import com.example.glucoapp.food.AddFoodActivity;
import com.example.glucoapp.food.FoodHistoryActivity;

public class MenuActivity extends AppCompatActivity {

    UserDTO user = new UserDTO();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
        user = (UserDTO) getIntent().getSerializableExtra("user");
        TextView title = findViewById(R.id.titleText);
        title.setText("witaj " + user.getUsername());
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }


    public void onClickAddMeasure(View view) {
        Intent intent = new Intent(this, AddMeasureActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    public void onClickDayMeasuresBtn(View view) {
        Intent intent = new Intent(this, MeasureHistoryActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    public void onClickMeasuresChartBtn(View view) {
        Intent intent = new Intent(this, MeasureChartActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }


    public void addFoodBtn(View view) {
        Intent intent = new Intent(this, AddFoodActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    public void onClickFoodHistory(View view) {
        Intent intent = new Intent(this, FoodHistoryActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }

    public void onClickBack(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("user", user);
        this.startActivity(intent);
    }
}
